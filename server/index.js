'use strict';
const express = require('express'); // express
const bodyParser = require('body-parser'); //bodyparser
const cors = require('cors'); //cors

const app = express(); // initialize express

// Middleware dependencies
app.use(bodyParser.json());
app.use(cors());

const posts = require('./routes/api/posts'); 

app.use('/api/posts', posts);

// Handle production environment
//if (process.env.NODE_ENV === 'production') {
  // Static folder 
  app.use(express.static(__dirname + '/public/'));

  // Handle single page application (SPA)
  app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'));
//}

const port = process.env.PORT || 5000; //production port or localhost use port 5000

app.listen(port, () => console.log(`Server started on port ${port}`)); //logs to console used port
