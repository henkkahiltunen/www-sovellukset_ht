'use strict';
const express = require('express'); // express
const mongodb = require('mongodb'); // used database mongodb

const router = express.Router();

// Get Posts from database
router.get('/', async (req, res) => {
  const posts = await loadPostsCollection();
  res.send(await posts.find({}).toArray());
});

// Add Post to database
router.post('/', async (req, res) => {
  const posts = await loadPostsCollection();
  await posts.insertOne({
    text: req.body.text,
    createdAt: new Date()
  });
  res.status(201).send(); //response from backend
});

// Delete Post from database
router.delete('/:id', async (req, res) => {
  const posts = await loadPostsCollection();
  await posts.deleteOne({ _id: new mongodb.ObjectID(req.params.id) });
  res.status(200).send(); //response from backend
});

//database connection using async.
async function loadPostsCollection() {
  const client = await mongodb.MongoClient.connect(
    'mongodb://henkka123:henkka123@ds161092.mlab.com:61092/www_sovellukset',
    {
      useNewUrlParser: true
    }
  );

  return client.db('www_sovellukset').collection('posts');
}

module.exports = router;
